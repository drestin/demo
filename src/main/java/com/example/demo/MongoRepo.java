package com.example.demo;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface MongoRepo extends MongoRepository<Visit, Long> {

    public Optional<Visit> findById(Long id);
}
