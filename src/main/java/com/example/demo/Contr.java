package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class Contr {

    @Autowired
    MongoRepo repo;

    @GetMapping("add/{id}")
    public void addVisit(@PathVariable Long id){

        Visit v = new Visit(id, 0);
//        v.id = id;
//        v.numberOfVisits = 0;
          repo.save(v);
    }

    @GetMapping("id/{arg}")
    public String method(@PathVariable  Long arg){
        Visit v = repo.findById(arg).get();
        v.numberOfVisits += 1;
        repo.save(v) ;
        return "DZIALA  " + v.toString();
    }
}
