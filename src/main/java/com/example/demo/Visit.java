package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.data.annotation.Id;

@AllArgsConstructor
@Builder
public class Visit {

    @Id
    public Long id;

    public int numberOfVisits;

    public String toString(){
        return "Number of visits: " + numberOfVisits;
    }
}
