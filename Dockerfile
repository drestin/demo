FROM adoptopenjdk/maven-openjdk11

WORKDIR /home/michal/IdeaProjects/demo

COPY . .

CMD ["java","-jar","./target/demo-0.0.1-SNAPSHOT.jar"]
